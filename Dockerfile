FROM node:latest

RUN mkdir -p /nodejs
WORKDIR /nodejs

COPY package.json /nodejs
RUN npm install

COPY . /nodejs